$(function() {

	// Header slider code
    
    $('.slider-nav_inner').on('click', function (e) {
        
        e.preventDefault();
        
        var $this = $(this),
            otherLinks = $('.slider-nav_inner'),
            slides = $('.slide'),
            itemPos = $this.index(),
            state = $this.hasClass('active-nav');
        
        slides.removeClass('active-slide');
        otherLinks.removeClass('active_nav');
        
        $this.addClass('active_nav');
        slides.eq(itemPos).addClass('active-slide');
        
    })

});
